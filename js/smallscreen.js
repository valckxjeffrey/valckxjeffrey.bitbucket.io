var $window = $(window), $bar = $('.yellowbar');

$window.resize(function me () {
    var small = $window.height() < 600 || $window.width() < 800;

    $bar.css('display', small ? 'block' : 'none');

    return me;
}());