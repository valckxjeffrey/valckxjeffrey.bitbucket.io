function myMap() {

    var myLatLng = { lat: 51.324390, lng: 3.975340 };

    var mapOptions = {
        center: myLatLng,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true

    }

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    marker.setMap(map);
}