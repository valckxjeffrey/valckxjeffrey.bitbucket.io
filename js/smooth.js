$(document).ready(function(){
    // Wanneer een link wordt aangeklikt, voer functie uit
    $("a").on('click', function(event) {
  
        if (this.hash !== "") {
        
        event.preventDefault();
  
        var hash = this.hash;
  
        // Gebruik jQuery's Animate-functie om smooth scrolling toe te passen
        // Het getal (in dit geval 800) staat gelijk aan het aantal milliseconden dat het duurt om naar het specifieke gebied te scrollen
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){
     
          // Hashtag toevoegen aan URL als scrollen voorbij is
          window.location.hash = hash;
        });
      } // Einde if
    });
  });

  $("a[href^=#]").on("click", function(e) {
    e.preventDefault();
    history.pushState({}, "", this.href);
});